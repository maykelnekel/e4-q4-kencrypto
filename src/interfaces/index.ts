export interface quoteOF {
  [ḱey: string]: {
    price: number;
    last_updated: string;
  };
}

export interface QuotesLatest {
  [key: string]: {
    id: number;
    name: string;
    symbol: string;
    slug: string;
    date_added: string;
    last_updated: string;
    quote: quoteOF;
  };
}

export interface QuotesConversion {
  id: number;
  symbol: string;
  name: string;
  amount: number;
  last_updated: string;
  quote: quoteOF;
}

export interface DataQuotesLatest {
  data: QuotesLatest;
}

export interface DataQuotesConversion {
  data: QuotesConversion;
}