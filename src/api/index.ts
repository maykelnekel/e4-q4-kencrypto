import axios, { AxiosInstance, AxiosResponse } from "axios";
import dotenv from "dotenv";
import {formatConversion, formatLatestQuote} from '../services/formater'

dotenv.config();
const key = process.env.API_KEY || "";

export class Kencrypto {
  baseURL: string = "https://pro-api.coinmarketcap.com/v1/";
  axiosInstace: AxiosInstance;
  apiKey: string = key;

  constructor() {
    this.axiosInstace = axios.create({
      baseURL: this.baseURL,
      headers: { "X-CMC_PRO_API_KEY": this.apiKey },
    });
  }

  async quotes(symbols: Array<string>) {
    const requestUrl = `cryptocurrency/quotes/latest?symbol=${symbols.join(
      ","
    )}`;
    try {
      const response: AxiosResponse<any, any> = await this.axiosInstace.get(
        requestUrl
        );
        return formatLatestQuote(response.data.data, symbols);

      } catch (err) {
        if(axios.isAxiosError(err)){

          return err.response?.data
        }
      }
  }

  async conversion(symbol: string, amount: number, convert: Array<string>) {
    const requestUrl = `tools/price-conversion?amount=${amount}&symbol=${symbol}&convert=${convert}`;
    try {

      const response: AxiosResponse<any, any> = await this.axiosInstace.get(
        requestUrl
        );
        
        return formatConversion(response.data.data, convert) ;
      } catch (err) {
        if(axios.isAxiosError(err)){

          return err.response?.data
        }
      }
  }
}
