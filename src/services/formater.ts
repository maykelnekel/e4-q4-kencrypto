import { AxiosResponse } from "axios";
import { DataQuotesConversion, DataQuotesLatest, QuotesConversion, QuotesLatest } from "../interfaces";

export const formatLatestQuote = (
  response: QuotesLatest,
  symbols: Array<string>
): DataQuotesLatest => {
  let output: DataQuotesLatest;

  symbols.forEach((symbol) =>
    output === undefined
      ? (output = {
          data: {
            [symbol]: {
              id: response[symbol].id,
              name: response[symbol].name,
              symbol: response[symbol].symbol,
              slug: response[symbol].slug,
              date_added: response[symbol].date_added,
              last_updated: response[symbol].last_updated,
              quote: response[symbol].quote,
            },
          },
        })
      : (output = {
          data: {
            ...output.data,
            [symbol]: {
              id: response[symbol].id,
              name: response[symbol].name,
              symbol: response[symbol].symbol,
              slug: response[symbol].slug,
              date_added: response[symbol].date_added,
              last_updated: response[symbol].last_updated,
              quote: response[symbol].quote,
            },
          },
        })
  );

  //   usando ts-ignore pois o TS alerta que a variável output está sendo utilizada antes de ser declarada, pois realmente ela está como undefined na linha 8
  //  porém ela realmente só será preenchida com a iteração do forEach. E por conta da tipagem, não posso declarar que `outpu : Quotes | undefined`
  // @ts-ignore
  return output;
};

export const formatConversion = (
  response: QuotesConversion,
  symbols: Array<string>
): DataQuotesConversion => {
  let output: DataQuotesConversion;

  symbols.forEach((symbol) =>
    output === undefined
      ? (output = {
          data: {
            id: response.id,
            symbol: response.symbol,
            name: response.name,
            amount: response.amount,
            last_updated: response.last_updated,
            quote: {
              [symbol]: {
                price: response.quote[symbol].price,
                last_updated: response.quote[symbol].last_updated,
              }
            }
          }
        })
      : (output = {
          data: {
            id: response.id,
            symbol: response.symbol,
            name: response.name,
            amount: response.amount,
            last_updated: response.last_updated,
            quote: {
              ...output.data.quote,
              [symbol]: {
                price: response.quote[symbol].price,
                last_updated: response.quote[symbol].last_updated,
              }
            }
          }
        })
  );

  //   usando ts-ignore pois o TS alerta que a variável output está sendo utilizada antes de ser declarada, pois realmente ela está como undefined na linha 8
  //  porém ela realmente só será preenchida com a iteração do forEach. E por conta da tipagem, não posso declarar que `outpu : Quotes | undefined`
  // @ts-ignore
  return output;
};
